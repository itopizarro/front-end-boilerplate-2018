# Getting started
* Install prescribed  version of [nodejs](https://nodejs.org) with Node Version Manager
  * Run `nvm install`, then `nvm use`
* Install necessary node modules with [Node Package Manager](https://www.npmjs.com/)
  * Run `npm install`
* Update the `~/project.config.json` to reflect project directory structure (more below)
  * __WARNING__ the `.gitignore` is set to ignore `./ui/dist`; if your installation isn't in the root directory (`./`) of your project, __or__ you're using custom directories, you'll need to update the `.gitignore` to reflect that. Otherwise, Git will track your output and you'll get an __unrelenting barrage of conflicts__
* Start Gulp (more below)

### project.config.json
The `project.config.json` defines the location of all the files/directories referenced in `gulpfile.js` and in `webpack.config.js`. 

| Property  | Description   |
| --- | --- |
| `theme` | `Object`: Collects details relating to the project's __Theme__ location (if there is one) |
| `source` | `Object`: Collects details relating to the project's __Source__ directories/files |
| `asset` | `Object`: Collects details relating to the project's __Asset__ (compiled/output) directories/files |
| `*.path` | `String`: path to the root directory (e.g. `foo/bar/`, `./`) | 
| `*.pattern` | `String`: [glob matching pattern](https://stackoverflow.com/a/26506124/5796134) |
| `*.output` | `String`: desired filename for resulting output (e.g. `compiled-styles.css`) | 
| `source.scripts.build` | `Object`: Collects details about the project's __Build__ process directories/files |
| `source.scripts.build.entry` | `String`: The file that acts as an entry point for [WebPack's Single Entry Point](https://webpack.js.org/concepts/entry-points/#single-entry-shorthand-syntax) |


## Gulp
| Command                   | Description   | Watcher |
| ---                       | ---           | ---                   |
| `gulp default`            | Copies __Font__ files, Compiles __SASS__, Builds (using the _development_ profile) __JS__, Copies/compresses __Images__, and __sets a watcher__ to automatically run those processes when changes are made to the respective source files (defined in the `source` property of `project.config.json`), with the output going to the distribution directory (defined in the `asset` property of `project.config.json`)  | Yes. All the things. |
| `gulp dev`                | This is an alias of `gulp default`  | Also Yes |
| `gulp qa`                 | Copies __Font__ files, Compiles __SASS__, Builds (using the _development_ profile) __JS__, Copies/compresses __Images__ | No |
| `gulp build`              | Copies __Font__ files, Compiles __SASS__, Builds (using the _production_ profile) __JS__, Copies/compresses __Images__ | No |
| `gulp prod`               | This is an alias of `gulp build` | Also No |
| `gulp scriptless-watch`   | Built, essentially, for when you need to Watch/Compile __SASS__ but don't need WebPack to build the JS side of things, it also Copies __Font__ files for&hellip; reasons. | Yes, but on fewer directories |
